package cn.techcave.chat.jpa.repository

import cn.techcave.chat.jpa.domain.Author
import cn.techcave.chat.jpa.domain.Post
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface AuthorRepo: JpaRepository<Author, Long> {
}