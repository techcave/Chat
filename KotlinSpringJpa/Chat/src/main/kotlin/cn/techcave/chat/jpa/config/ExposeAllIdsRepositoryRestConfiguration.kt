package cn.techcave.chat.jpa.config
import org.springframework.data.rest.core.config.EnumTranslationConfiguration
import org.springframework.data.rest.core.config.MetadataConfiguration
import org.springframework.data.rest.core.config.ProjectionDefinitionConfiguration
import org.springframework.data.rest.core.config.RepositoryRestConfiguration

/**
 * 重载暴露所有
 */
class ExposeAllIdsRepositoryRestConfiguration(prjConf: ProjectionDefinitionConfiguration, metaConf: MetadataConfiguration, enumConf: EnumTranslationConfiguration) : RepositoryRestConfiguration(prjConf, metaConf, enumConf) {
    override fun isIdExposedFor(domainType: Class<*>): Boolean = true
}