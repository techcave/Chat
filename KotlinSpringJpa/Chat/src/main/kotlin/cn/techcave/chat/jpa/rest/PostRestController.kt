package cn.techcave.chat.jpa.rest

import cn.techcave.chat.jpa.service.PostService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("api/post")
class PostRestController {

    @Autowired
    lateinit var postSvc:PostService

    @GetMapping("findAuthor")
    fun findAuthor(@PageableDefault(value = 15, sort = arrayOf<String>("createDate" ), direction = Sort.Direction.DESC) page:Pageable?) = postSvc.findAuthor(page)
}