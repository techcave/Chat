package cn.techcave.chat.jpa.domain

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
open class BaseEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long? = null,
    @CreatedDate
    var createDate:Date? = null,
    @LastModifiedDate
    var updateDate:Date? = null,
    @CreatedBy
    var createBy:String? = null,
    @LastModifiedBy
    var lastModifiedBy:String? = null
)