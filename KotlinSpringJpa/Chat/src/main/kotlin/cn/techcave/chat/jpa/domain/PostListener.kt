package cn.techcave.chat.jpa.domain

import org.springframework.stereotype.Component
import javax.persistence.PostPersist
import javax.persistence.PrePersist

@Component
class PostListener {

    @PrePersist
    fun beforeSave(post:Post) {
        println("run before save")
        println(post)
    }

    @PostPersist
    fun afterSave(post:Post) {
        println("run after save")
        println(post)
    }
}