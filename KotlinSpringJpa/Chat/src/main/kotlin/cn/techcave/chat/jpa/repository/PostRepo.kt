package cn.techcave.chat.jpa.repository

import cn.techcave.chat.jpa.domain.Post
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.querydsl.QueryDslPredicateExecutor
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@RepositoryRestResource
//interface PostRepo:JpaRepository<Post, Long>, JpaSpecificationExecutor<Post>, QueryDslPredicateExecutor<Post> {
interface PostRepo:JpaRepository<Post, Long>, JpaSpecificationExecutor<Post> {

    /**
     * 使用方法名根据标题和内容类匹配
     */
    fun findByTitleContainingOrContentContaining(title:String, content:String):List<Post>

    /**
     * 使用@Query注解，手动编写查询语句
     */
    @Query("select post from Post post where post.author.name = ?1")
    fun findByAuthor(autherName:String): List<Post>

    /**
     * 使用方法名排序：创建时间升序,注意前面那个By，查询方法的第一个By表示路径表达式的开始，没有这个By会报错。
     */
    fun findByOrderByCreateDateDesc():List<Post>

    @Transactional(timeout = 20)
    override fun findAll(): MutableList<Post>
}