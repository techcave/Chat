package cn.techcave.chat.jpa.domain

import org.springframework.data.domain.AuditorAware
import java.util.*

class PostAuditorAware : AuditorAware<String> {
    override fun getCurrentAuditor(): String {
        return "孙亖 ${Random().nextInt()}"
    }
}