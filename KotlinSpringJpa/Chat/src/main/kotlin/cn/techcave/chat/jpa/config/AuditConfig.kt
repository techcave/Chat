package cn.techcave.chat.jpa.config

import cn.techcave.chat.jpa.domain.PostAuditorAware
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.domain.AuditorAware


/**
 * 配置打开审计
 */
@Configuration
@EnableJpaAuditing
class AuditConfig {

    @Bean
    fun auditorProvider(): AuditorAware<String> {
        return PostAuditorAware()
    }
}