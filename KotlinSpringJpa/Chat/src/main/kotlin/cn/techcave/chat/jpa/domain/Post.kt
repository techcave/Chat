package cn.techcave.chat.jpa.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import javax.persistence.*

@Entity
@EntityListeners(PostListener::class)
data class Post(
        var title: String? = null,
        @Lob
        var content: String? = null,
        @JsonBackReference
        @ManyToOne
        @JoinColumn(name="authorId")
        var author: Author? = null,
        @Transient
        var authorId:Long? = null
) : BaseEntity()