package cn.techcave.chat.jpa

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class JpaApplication

fun main(args: Array<String>) {
    SpringApplication.run(JpaApplication::class.java, *args)
}
