package cn.techcave.chat.jpa.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.service.Tag
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
 * 配置Swagger2
 */
@Configuration
@EnableSwagger2
@Import(SpringDataRestConfiguration::class)//repositoryRestResource API
class SpringfoxConfiguration {
    @Bean
    fun createRestApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .tags(Tag("Post Entity", "文章帖子"),
                        Tag("Author Entity", "作者信息"))
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
//                .apis(RequestHandlerSelectors.basePackage("org.springframework.data.rest.webmvc"))
                .paths(PathSelectors.any())
                .build()
    }

    private fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
                .title("Spring Data Jpa 的 Chat 演示 API")
                .description("Spring Data Jpa 的 Chat 演示 API")
                .termsOfServiceUrl("http://localhost:8080/")
                .contact(Contact("孙亖", "http://blog.techcave.cn", "techcave@foxmail.com"))
                .version("1.0")
                .build()
    }
}