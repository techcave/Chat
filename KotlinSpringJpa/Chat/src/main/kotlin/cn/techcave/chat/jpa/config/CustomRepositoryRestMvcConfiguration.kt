package cn.techcave.chat.jpa.config
import org.springframework.context.annotation.Configuration
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration


//@Configuration
class CustomRepositoryRestMvcConfiguration : RepositoryRestMvcConfiguration() {
    override fun config(): RepositoryRestConfiguration {
        val config = super.config()
        return ExposeAllIdsRepositoryRestConfiguration(config.projectionConfiguration, config.metadataConfiguration, config.enumTranslationConfiguration)
    }
}