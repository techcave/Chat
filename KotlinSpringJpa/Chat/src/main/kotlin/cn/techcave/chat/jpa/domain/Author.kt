package cn.techcave.chat.jpa.domain

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.OrderBy

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
@JsonIdentityReference(alwaysAsId = true)
@Entity
data class Author(
        var name: String? = null,
        @JsonIgnore
        @OneToMany(mappedBy = "author")
        @OrderBy("createDate desc")
        var posts: List<Post>? = null
) : BaseEntity()