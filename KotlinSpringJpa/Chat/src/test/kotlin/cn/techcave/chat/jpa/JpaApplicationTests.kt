package cn.techcave.chat.jpa

import cn.techcave.chat.jpa.domain.Post
import cn.techcave.chat.jpa.repository.PostRepo
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class JpaApplicationTests {

	@Autowired
	lateinit var postRepo:PostRepo

	@Test
	fun newPost() {
		var post = Post()
		val r = Random().doubles()
		post.title = "新建标题_${r}"
		post.content = "文章内容——+￥｛｝${r}"
		postRepo.saveAndFlush(post)
		assert(post.id != null)
	}

}
