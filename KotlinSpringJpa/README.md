今天已经把分享的文章提交发布了，把代码也通过码云发布，分享内容目录如下，有兴趣的朋友欢迎参加：

*   [1\. 项目初始化](#1-项目初始化)
    *   [1.1 IDE选择](#11-ide选择)
    *   [1.2 新建项目](#12-新建项目)
    *   [1.3 代码组织](#13-代码组织)
    *   [1.4 数据库配置](#14-数据库配置)
*   [2\. 编写实体类](#2-编写实体类)
    *   [2.1 Kotlin Data Classes](#21-kotlin-data-classes)
    *   [2.2 Kotlin Proerties And Fields](#22-kotlin-proerties-and-fields)
    *   [2.3 实现帖子/文章实体类](#23-实现帖子文章实体类)
    *   [2.4 公共字段](#24-公共字段)
    *   [2.5 浏览数据库数据](#25-浏览数据库数据)
    *   [2.6 ER关系中解决JSON循环引用](#26-er关系中解决json循环引用)
    *   [2.7 忽略的字段](#27-忽略的字段)
*   [3\. 编写Repository](#3-编写repository)
*   [4\. 开发调试工具](#4-开发调试工具)
    *   [4.1 单元测试](#41-单元测试)
    *   [4.2 SwaggerUI](#42-swaggerui)
    *   [4.3 Postman](#43-postman)
*   [5\. 查询扩展](#5-查询扩展)
    *   [5.1 Query 方法](#51-query-方法)
    *   [5.2 Query By Example](#52-query-by-example)
    *   [5.3 Specification](#53-specification)
    *   [5.4 排序和分页](#54-排序和分页)
*   [6\. 审计和数据监听](#6-审计和数据监听)
    *   [6.1 是谁在什么时间修改数据](#61-是谁在什么时间修改数据)
    *   [6.2 触发器->监听](#62-触发器-监听)
*   [7\. 事务](#7-事务)
*   [8\. 总结](#8-总结)

内容简介：
Kotlin 是一个基于 JVM 的新的编程语言，Kotlin 可以编译成 Java 字节码，也可以编译成 JavaScript，方便在没有 JVM 的设备上运行。

Kotlin 已经成为 Android 原生开发的推荐首选语言，但传统领域的使用还存在着一定的争议，这次我们就感受下用 Kotlin 来开发一个 SpringBoot 是什么感觉。有 Java 基础的朋友将快速掌握 Kotlin 在 Spring 中的应用要点及其中会遇到的问题。

本次分享涉及实体建模，DAL、Service、Rest、测试等开发过程和遇到的问题，还有开发测试工具使用等讲解，源码将提交在码云，有兴趣朋友欢迎参加。

扫码参加：

![用Kotlin开发SpringBoot之Data JPA.jpg](./用Kotlin开发SpringBoot之Data JPA.jpg)


[阅读原文](http://blog.techcave.cn/2018/02/09/it/chat/%E4%BB%A3%E7%A0%81%E5%8F%91%E5%B8%83%EF%BC%9A%E7%94%A8-Kotlin-%E5%BC%80%E5%8F%91-SpringBoot-%E4%B9%8B-Data-JPA/)