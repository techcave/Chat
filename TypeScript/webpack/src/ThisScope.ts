export class ThisScope {
    txt:string = "hello this scope"
    cf1() {
        return function() {
            this.showInfo()
        }
    }

    cf2() {
        return () => {this.showInfo()}
    }

    cf3() {
        return function() {
            this.showInfo()
        }.bind(this)
    }

    showInfo() {
        console.log(this.txt)
    }
}