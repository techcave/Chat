import {ThisScope} from './ThisScope'
function greeter(person:string) {
    return "Hello, " + person;
}

let user = "TypeScript User";

document.body.innerHTML = greeter(user);

let thisScope = new ThisScope()
thisScope.cf1()()

// while(1) {
//     thisScope.txt = "hhhhhh"
// }